import Vue from 'vue'
import App from './App.vue'
import VueAnalytics from 'vue-analytics'
import store from './store'
import '@mdi/font/css/materialdesignicons.css'
import lineClamp from 'vue-line-clamp'
import router from './router'
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false

Vue.use(VueAnalytics, {
	router,
	id () {
		return ''
	}
})

Vue.use(lineClamp, { })

Vue.mixin({
    methods: {
        UserHasRole (role) {
            var user = this.$store.state.ui.user
            if (!user || !user.roles) {
                return false
            }
            return user.roles.includes(role)
        }
    }
})


new Vue({
    store,
    router,
    vuetify,
    render: h => h(App)
}).$mount('#app')
