import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Dashboard from '../views/Dashboard.vue'
import Map from '../views/Map.vue'
import Events from '../views/Events.vue'
import Library from '../views/Library.vue'
import Settings from '../views/Settings.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: {
		title: "UAPTN: UAP Tracking Network"
    },
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: Dashboard,
    meta: {
		title: "UAPTN: UAP Tracking Network"
    },
   },
  {
    path: '/map',
    name: 'map',
    component: Map,
    meta: {
		title: "UAPTN: UAP Tracking Network"
    },
   },
   {
      path: '/events',
      name: 'events',
      component: Events,
      meta: {
		title: "UAPTN: UAP Tracking Network"
      },
  },
   {
      path: '/library',
      name: 'library',
      component: Library,
      meta: {
		title: "UAPTN: UAP Tracking Network"
      },
  },
   {
      path: '/settings',
      name: 'settings',
      component: Settings,
      meta: {
		title: "UAPTN: UAP Tracking Network"
      },
  },
]

const router = new VueRouter({
  routes,
  mode: 'history'
})

export default router
