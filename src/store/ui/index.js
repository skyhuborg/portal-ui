/* eslint-disable */
const { UiClient } = require('proto-portal-ui-web/Portal-uiServiceClientPb')
const { GetNodeStatusReq, AuthReq, AuthType} = require('proto-portal-ui-web/portal-ui_pb')

var client = new UiClient(process.env.VUE_APP_GRPC_ENDPOINT)

/* elint-disable */
export default {
    namespaced: true,
    state: {
        NodeStatus: [],
		authenticated: false,
        authToken: null,
        user: null,
        status: "",

    },
    mutations: {
        NodeStatus(store, NodeStatus) {
            store.NodeStatus = NodeStatus
        },
		authenticated (store, state) {
            store.authenticated = state
        },
        authToken (store, token) {
            store.authToken = token
            localStorage.setItem('authToken', token)
        },
        user (store, user) {
            if (user) {
                user.roles = user.roles.split(",")
            }
            store.user = user
        },
        status (store, msg) {
            store.status = msg
        }
    },
    actions: {
        login (store, {username, password}) {
            var request = new AuthReq()
            var metadata = {}

            request.setRequesttype(AuthType.AUTH_LOGIN)
            request.setUsername(username)
            request.setPassword(password)

            client.auth(request, metadata, function(err, response) {
                if (!response) {
                    return;
                }

                if (err) {
                } else {
                    var obj = response.toObject()
                    if (obj.status === 'success') {
                        store.commit('authToken', obj.token)
                        store.commit('user', obj.user)
                        store.commit('status', obj.status)
                        store.commit('authenticated', true)
                    } else {
                        store.commit('authToken', null)
                        store.commit('user', null)
                        store.commit('status', obj.status)
                        store.commit('authenticated', false)
                    }
                }
            })
        },
        logout (store) {
            var request = new AuthReq()
            var metadata = {}

            let authToken = localStorage.getItem('authToken');

            request.setRequesttype(AuthType.AUTH_LOGOUT)
            request.setToken(authToken)

            client.auth(request, metadata, function(err, response) {
                if (!response) {
                    return;
                }

                if (err) {
                } else {
                    var obj = response.toObject()
                    store.commit('authToken', null)
                    store.commit('status', obj.status)
                    store.commit('user', null)
                    store.commit('authenticated', false)
                }
            })
        },
        loginCheck (store) {
            var request = new AuthReq()
            var metadata = {}

            let authToken = localStorage.getItem('authToken');

            request.setRequesttype(AuthType.AUTH_CHECK)
            request.setToken(authToken)

            client.auth(request, metadata, function(err, response) {
                if (!response) {
                    return;
                }

                if (err) {
                } else {
                    var obj = response.toObject()
                    if (obj.status === 'success') {
                        store.commit('authToken', obj.token)
                        store.commit('user', obj.user)
                        store.commit('authenticated', true)
                    } else {
                        store.commit('authenticated', false)
                        store.commit('user', false)
                    }
                }
            })
        },
        GetNodeStatus (store) {
            var request = new GetNodeStatusReq()

            var metadata = { authToken: store.state.authToken }
            client.getNodeStatus(request, metadata, function(err, response) {
					/* eslint-disable */
                if (err) {
                    store.commit('NodeStatus', null)
                } else {
                    var res = response.toObject()
                    store.commit('NodeStatus', res.statusList)
                }
            })
        },
	},
}



