module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  "configureWebpack": {
    "devServer": {
      "proxy": {
        "/file": {
          "target": "http://localhost:2015",
          "pathRewrite": {
            "^/file": ""
          }
        }
      }
    }
  }
}